# Chiquiloso's Fedora

⚠️**Disclaimer: the project is under revision due to the release of Fedora 41. Also, the files under /dev are not ready to use. A release version will be available soon.**⚠️

&nbsp;

Hi there and welcome to my Fedora Workstation setup GitLab project! 👋

This project compiles the system configuration and list of applications (both GUI and CLI) that I usually use to prepare Fedora Workstation for game development, multimedia content creation (images, audio, video, and web), gaming, and general computer science projects (data analysis, AI, ML, application development, etc.).

Specifically, this project elaborates upon the areas of:
- [Package management.](#pac-man) 
- [System updates.](#sys-up)
- [Desktop environments.](#de)
- [Display servers.](#ds)
- [System monitors.](#sys-mon)
- [Fan hubs and RGB lights.](#rgb)
- [Storage](#raid)
- [Video Games.](#vg)
- [Artificial Intelligence.](#ai)
- [Applications.](#app)

I made this project for my own sake, so I can keep track of all the things I do to my system in case I have to restore it, or when making a fresh installation of the OS. However, I hope it comes handy to whoever gets to use it! 😄

Cheers!

*Chiquiloso.*

&nbsp;

> ℹ️ **Disclaimer**: The latest version of Fedora, in which the contents of this project have been tested, is **Fedora Workstation 40**. 

> 💡 **Tip**: The contents of this project should not be understood as a tutorial. Instead, it should be seen as a reference guide to set up and configure Fedora Workstation. Despite this, the project does present some step-by-step video tutorials in several sections of it, as complementary material.

&nbsp;

## 📦 Package Manager <a name="pac-man"></a>

[DNF](https://docs.fedoraproject.org/en-US/quick-docs/dnf/) is the current package manager used by Fedora Workstation. It handles [RPM](https://rpm.org/index.html) packages by default, and it has been quite stable and efficient in my experience. Despite this, DNF can be further upgraded to improve its performance and expand its packages sources.

&nbsp;

### Improving DNF Performance
To access the DNF configuration file, we can use [nano](https://www.nano-editor.org/) (a CLI text editor that comes by default in Fedora Workstation) using the following terminal command:

    sudo nano /etc/dnf/dnf.conf

There, we can modify a set of parameters to improve and customize the behavior of DNF. First, we can identify the fastest source to download packages from, by adding:

    fastestmirror=True

> 💡 **Tip**: The previous parameter allows DNF to search for the best possible mirror, in terms of **Latency** (i.e. the actual time it takes to get an answer from the server). However, this does not necessarily translate to a faster download speed. For more information, check the [DNF Configuration Reference](https://dnf.readthedocs.io/en/latest/conf_ref.html#options-for-both-main-and-repo).

In case your internet connection is not very fast, you can limit the amount of packages that can be downloaded simultaneously with:

    max_parallel_downloads=10

Also, we can cache the downloaded packages so future upgrades are done quicker:

    keepcache=True

If you go for this option, remember that you can also use the following terminal commands to clean the stored cache from DNF:

    sudo dnf clean dbcache

or

    dnf clean all

&nbsp;

### Enabling RPM Fusion
[RPM Fusion](https://rpmfusion.org/) is an extension of the default Fedora repository, that expands the amount of packages available for the system. To enable RPM Fusion repositories (both free and non-free), we must execute the following terminal commands, as suggested in their [official configuration guide](https://rpmfusion.org/Configuration):

    sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

and

    sudo dnf config-manager --enable fedora-cisco-openh264

We can also allow [Discover](https://apps.kde.org/discover/) (KDE's default software center) to browse and display RPM fusion repositories, by executing the following command:

    sudo dnf groupupdate core

&nbsp;

### Enabling Proprietary Multimedia Codecs and Utilities
Since Fedora Workstation is an OS that prioritizes the use of open-source packages in its default setup, many multimedia codecs and other utilities (e.g. fonts) that are commonly needed in sites like Twitch or YouTube, are not initially installed in the system.

To install these missing components, we can run the following terminal commands:

> ⚠️ **Warning:** RPM Fusion must be installed before executing these commands.

    sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
    
    sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
    
    sudo dnf install lame\* --exclude=lame-devel
    
    sudo dnf groupupdate sound-and-video

    sudo dnf group upgrade --with-optional Multimedia

To install Microsoft fonts (in case you have to work with MS Office files), we can use the following terminal commands:

    sudo dnf install curl cabextract xorg-x11-font-utils fontconfig

    sudo rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm

&nbsp;

### Enabling Flatpaks
[Flatpak](https://www.flatpak.org/) is a package format that focuses on system integrity, by isolating program dependencies from the OS native packages. Flatpak is recommended for programs that do not need to manipulate or access OS binaries. To enable Flatpak in Fedora Workstation, we execute the following terminal command:

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

&nbsp;

## 📥 System Updates <a name="sys-up"></a>
With RPM Fusion and Flatpak installed, we can go ahead and update our system.

The following list compiles some of the common terminal commands to perform system updates:

- To update the system's RPM packages:

      sudo dnf upgrade

- To remove unused RPM packages in you system:

      sudo dnf autoremove

- To update Flatpak packages:

      flatpak update

&nbsp;

## 🖥️ Desktop Environments <a name="de"></a>

Fedora Workstation can be installed with different desktop environments, including [XFCE](https://xfce.org/), [Cinnamon](https://wiki.archlinux.org/title/Cinnamon), [Mate](https://mate-desktop.org/), [SWAY tilling window manager](https://swaywm.org/), among many others. However, [KDE Plasma](https://fedoraproject.org/spins/kde/) and [Gnome](https://fedoraproject.org/workstation/) are the desktop environments I commonly use.

I use **KDE Plasma** on my personal computers and it is, with difference, **my favorite desktop environment**. Despite this, I also use Gnome on my work computers, mainly, to keep track of the changes and improvements they do on their platform.

&nbsp;

### KDE Plasma
[KDE Plasma](https://kde.org/plasma-desktop/) offers all sort of customization options by default, and (most of the time) no additional add-ons are required to customize the looks and behavior of Fedora Workstation. Based on my experience, Fedora running KDE Plasma has been the most stable experience I have ever had in a Linux environment and, even if its looks are not as modern as Gnome, its functionality, adaptability, and strong focus on productivity won me over. Kudos KDE team! 👍

For the most part, I stick with the default configuration of KDE, and I only modify small aspects of its default appearance.

> 💡 **Tip**: KDE Plasma offers a lot of customization options (seriously!), and it can feel pretty daunting when you are learning how to use it. I advise you to take your time and explore all the options available to you, and to try what works and what doesn't. Also, there are many guides that you can browse, to [customize](https://itsfoss.com/kde-customization/) or to [theme](https://itsfoss.com/properly-theme-kde-plasma/) KDE Plasma.

A single floating panel *plasmoid* (i.e. the name of the widgets that are native in KDE Plasma) is created in the top part of the desktop, covering its entire width. The panel is divided into three segments (called here left, center, and right) using two panel spacer plasmoids.

On the left segment, there is an [icons-only task manager](https://userbase.kde.org/Plasma/Tasks#Icons_only_Task_Manager), that I use to pin the applications I want to launch with the keyboard shortcut *super+number* (i.e. "Windows key" plus a number key).

On the center segment, I use three plasmoids. The first one is a [Kickoff application launcher](https://userbase.kde.org/Plasma/Kickoff), that uses my logo as its icon. The second one is a digital clock plasmoid that I configured to display not only the time, but the calendar as well. I added holidays relevant to the country I live in, and multiple time zones that are relevant for me and my work. Last, there is a weather report plasmoid that connects to the BBC Weather database.

On the right segment, there are 2 plasmoids. The first one is a network speed plasmoid, that monitors the traffic (upload and download) on my internet connection. The other one is a default system tray plasmoid.

Last, I like to keep certain aesthetic consistency among different components of my system, without resourcing to external customized theme packages. Therefore, system colors, wallpapers, plasmoid colors, and RGB lights tend to match between each other.

This is how my desktop look like with KDE Plasma:

![Chiquiloso's KDE Plasma Desktop](res/desktop.png "Chiquiloso's KDE Plasma Desktop")

&nbsp;

### Gnome
After a fresh Fedora Workstation install, Gnome looks very modern, but it feels rather bare-bones in its functionality. This is because Gnome bases its customization on a modular architecture based on *shell extensions*: additional components that expand over the default functionality of Gnome. So, to customize Gnome and to browse and install shell extensions, two additional programs need to be locally installed:

- **[Gnome Tweaks](https://itsfoss.com/gnome-tweak-tool/)**: Enables basic customization options for the desktop environment (color accents, window behaviors, button locations, etc.).
- **[Extensions](https://itsfoss.com/gnome-shell-extensions/)**: Manages and controls the different shell extensions running in your system. In order for extensions to be properly installed, not only the *Extensions* software is needed, but also a browser-specific add-on for *Gnome Extensions* (e.g. [Gnome Shell Integration for Firefox](https://addons.mozilla.org/en-US/firefox/addon/gnome-shell-integration/)). In this way, the online catalog of shell extensions can be directly installed from your browser. 

The shell extensions that I commonly use in a Fedora Workstation are:

- [Caffeine](https://extensions.gnome.org/extension/517/caffeine/): Disables auto-suspend of the OS.
- [Vitals](https://extensions.gnome.org/extension/1460/vitals/): A system monitor for your main panel.
- [Blur my shell](https://extensions.gnome.org/extension/3193/blur-my-shell/): Blurs the wallpaper and uses it as a background for the activity panel.
- [Custom accent colors](https://extensions.gnome.org/extension/5547/custom-accent-colors/): More color options for the windows and GUI elements.
- [Dash to dock](https://extensions.gnome.org/extension/307/dash-to-dock/): Allows applications pinned to the dash panel to be visible in the desktop, as an application dock.
- [Just perfection](https://extensions.gnome.org/extension/3843/just-perfection/): Allows to modify the position, behavior and availability of icons and GUI elements.
- [User themes](https://extensions.gnome.org/extension/19/user-themes/): Installs user-created themes in your system.
- [Tray Icons Reloaded](https://extensions.gnome.org/extension/2890/tray-icons-reloaded/): Displays tray icons, and their respective drop-down menus, in the main panel.
- [Sound input and output chooser](https://extensions.gnome.org/extension/906/sound-output-device-chooser/): Allows the selection of sound I/O interfaces from the tray icon menu.

This is how Gnome looks in my Fedora Workstation:

![Chiquiloso's Gnome Desktop](res/desktop_gnome.png "Chiquiloso's Gnome Desktop")

&nbsp;

## 📺 Display Server <a name="ds"></a>

Fedora Workstation uses [Wayland](https://wayland.freedesktop.org/) as the default [display server](https://itsfoss.com/display-server/) for the OS.

In my opinion, Wayland is amazing!. It feels snappy and it is very efficient (especially when frame rendering and refresh rates are important). So, if you plan on gaming or developing and testing games, Wayland will not disappoint!

There is also a very interesting GitHub repository (now archived 🙁) that compiles a [list of curated utilitites that are being developed for Wayland](https://github.com/natpen/awesome-wayland), in case you are looking for a particular tool. Kudos to Natalie Pendragon a.k.a. [napten](https://github.com/natpen) for this one! 👍

Nevertheless, tools to modify the display output (i.e. gamma, saturation, contrast, etc.) are almost non-existent in Wayland (to the best of my knowledge at the moment). So, if you plan to work on any task that require a high color-accuracy, you should use [X11](https://www.x.org/wiki/) instead.

Since we are in this topic, Fedora (and most of Linux distributions to be honest) have very limited options to modify different display options (e.g. color saturation, brightness, gamma, etc.) directly from the OS system settings. These options are better controlled using a terminal application called [xrandr](https://wiki.archlinux.org/title/Xrandr). Xrandr is a rather robust customization tool that allows you to tweak most of the display options of any screen in your system, using X11. It can change parameters globally (i.e. that affect all screens) or make changes only to specific screens. In this way, you can ensure that all your screens will have the same image quality, despite its manufacturer's initial configuration.

I have 2 different screens, an XP-Pen Artist 22 drawing tablet, and a Samsung SJ55W Ultrawide 34''. The picture quality of the XP-Pen monitor is outstanding, so I recreate it on my Samsung monitor using the following command:

    xrandr --output DisplayPort-0 --gamma 0.8:0.76:0.8 --brightness 1.0

In addition to this, I use an application called [Vibrant Linux](https://github.com/libvibrant/vibrantLinux) that allows me to modify the color saturation of each monitor in my system, when working in X11. It is developed by the [libvibrant team](https://github.com/libvibrant), and for the Samsung monitor, I use a color saturation of 110.

&nbsp;

## 📊 System Monitors <a name="sys-mon"></a>

System monitors are applications that allow you to review and visualize your hardware performance and usage.

By default, Fedora Workstation (and most Linux distributions) will feature a monitoring system called [top](https://www.man7.org/linux/man-pages/man1/top.1.html). Even if top is very useful, it looks rather outdated, and it does not showcase many metrics on its default configuration. Therefore, I prefer to use other solutions when monitoring the performance of my system.

&nbsp;

### CPU and System Monitor
My system monitor of choice is [bpytop](https://github.com/aristocratos/bpytop), developed by Jakob P. Liljenberg a.k.a. [aristocratos](https://github.com/aristocratos). It looks very modern, it is customizable, and I can have mouse interaction with it directly in the terminal! 

To install bpytop, just run the following terminal command:

    sudo dnf install bpytop

&nbsp;

### GPU Monitor

To monitor my GPU usage and performance, I use a program called [amdgpu_top](https://github.com/Umio-Yasuno/amdgpu_top), developed by [Umio Yasuno](https://github.com/Umio-Yasuno). As its name implies, it is meant to be used with Radeon graphics cards (I have a Radeon RX 6900XT).

By default, this application is not included in the Fedora repositories. Therefore, the easiest way to install amdgpu_top is to directly [download the RPM package from its GitHub repository](https://github.com/Umio-Yasuno/amdgpu_top?tab=readme-ov-file#installation). Once you have the RPM file, you can simply double-click on it, and Discover will install it for you.

&nbsp;

I am aware that KDE Plasma offers, natively, multiple plasmoids that can be customized to monitor your system. However, it looks much  cooler when we monitor the system through the terminal instead. Remember, [style over substance choom!](https://www.youtube.com/watch?v=YlyDJVYqfpA) 😎

Don't you believe me?, have a look:

![System Monitors](res/monitors.png "Chiquiloso's System Monitors")

&nbsp;

## 🌪️🌈 Fan Hubs and RGB Lights <a name="rgb"></a>

I use a [Corsair Commander Pro](https://www.corsair.com/us/en/p/custom-liquid-cooling/cl-9011110-ww/icue-commander-pro-smart-rgb-lighting-and-fan-speed-controller-cl-9011110-ww) to control 6 RGB fans that I have in my system. ICue, Corsair's official software for this device, does not offer Linux support. However, there are open-source alternatives that allow me to control this device in Fedora Workstation. 

&nbsp;

### Fan Hub

For the Commander Pro fan hub, I use [liquidctl](https://github.com/liquidctl/liquidctl), developed by the [liquidctl team](https://github.com/liquidctl). It is a terminal application designed, originally, to control water cooling systems, but that has expanded to support multiple hardware devices. To control the Commander Pro's fan hub, we can use the following set of terminal commands.

First, we need to initialize the Commander Pro with:

    sudo liquidctl -n 1 initialize

The `-n` parameter represents the device number assigned by the application (1 in the case of my Commander Pro). The device number can be retrieved using:

    liquidctl list

We can also verify the status of the system with the command:

    sudo liquidctl status

Once we have initialized the Commander Pro, we can set fan fixed speeds using the command: 

    sudo liquidctl -n 1 set sync speed 85

The `sync` parameter applies the changes to all fans in the fan hub, and `85` the selected percentage of the fan max speed. An adaptive fan speed can be setup, but it requires having thermal sensors to be plugged into the Commander Pro and... well... [ain't nobody got time for that!](https://www.youtube.com/watch?v=ydmPh4MXT3g) 💁🏿‍

&nbsp;

### RGB Lights

For the fan RGBs (and my mother board RGBs), I use [OpenRGB](https://openrgb.org/), a graphical application that can control many of the [most common RGB peripherals](https://openrgb.org/devices_0.9.html), developed by Adam Honse a.k.a. [CalcProgrammer1](https://github.com/CalcProgrammer1).

We can install it directly from Discover, or by using the terminal command:

    sudo dnf install openrgb

&nbsp;

## 💽 Storage <a name="raid"></a>

My system has two 4TB mechanical drives, where I store and backup my files (i.e. those that I am not currently working with).

I used the [mdadm](https://github.com/md-raid-utilities/mdadm/) utility to set up a software [RAID 1](https://en.wikipedia.org/wiki/Standard_RAID_levels#RAID_1) with those drives. The process is a bit long. So, instead of writing it here, I will simply share the [tutorial I used to set it up](https://www.linuxbabe.com/linux-server/linux-software-raid-1-setup). 

> 💡 **Tip**: This RAID setup persists even if you install a new OS, which makes it a very secure and practical way to back up your information.

Kudos to [LinuxBabe](https://www.linuxbabe.com/) for this guide! 👍

&nbsp;

## 🎮 Video Games <a name="vg"></a>

Call me naïve, but I truly believe **Linux is the future of gaming**. The accessibility of Linux distributions, together their high level of customization and optimization, makes of Linux an ideal platform for video games.

There is already a large number of commercial games that are available for Linux (for example, [check this list of Steam games that are natively supported in Linux](https://store.steampowered.com/linux)), together with many other [free and open source games](https://github.blog/2021-08-25-30-free-and-open-source-linux-games-part-1/).

If what you want to play is only available for Windows, Linux has you covered as well. To run Windows games on Linux, some tinkering may be needed. However, there are many tools that can do the heavy-lifting for you!

I will disclose some of the tools I use below. However, if you want a step-by-step tutorial in how to prepare your system for gaming on Linux, I can recommend [The Gaming on Linux Guide](https://www.youtube.com/watch?v=v9tb1gTTbJE) by The [Linux Experiment](https://www.youtube.com/@TheLinuxEXP). Kudos for this great video! 👍 

&nbsp;

### Proton
In order to play Windows games on Linux, the [Proton compatibility tool](https://github.com/ValveSoftware/Proton), developed by [Valve](https://www.valvesoftware.com/en/about),  must be enabled in the system via Steam. Two versions of Proton are recommended:

- The *latest-stable* is the customer-ready version tested by Valve (currently 9.0-1 as of May 2024).
- The *Proton-experimental* is a development version that may give you better game compatibility at the expense of system stability.

There is a third version of Proton called [Proton-GE-custom](https://github.com/GloriousEggroll/proton-ge-custom), which is a customized version of the Proton-experimental version. It is developed by the legendary programmer and Red Hat engineer Thomas Crider, a.k.a [Glorious Eggroll](https://github.com/GloriousEggroll). This version of Proton adds some features that are missing in the Proton-experimental version (for example AMD FSR, CUDA support for PhysX and NVAPI, etc.). It is a very good alternative when the official Proton versions do not seem to work on a particular game.

> 💡**Tip**: Other game launchers and applications will make use of the Proton libraries installed via Steam, to run Windows games as well.

In my experience, most games will run perfectly with either of these Proton versions. However, in case a specific game is giving you a hard time, you can always check the [ProtonDB Website](https://www.protondb.com/). ProtonDB is a community-based report page that documents which games are compatible with Proton, and what specific configuration they may require to run on Linux.

> ⚠️ **Warning**: Even if Proton allows many Windows games to run in Linux, it doesn't offer a 100% compatibility between these platforms. Based on my experience, the majority of cases when Windows games Crash in Linux, is due to other third party software required by the game (for example, kernel-level anticheats), and not the game itself.

&nbsp;

### Game Launchers

There are three game launchers that I use on my system: Steam, Lutris, and Heroic.

[Steam](https://store.steampowered.com/) is the most popular game launcher and marketplace in the world. It is developed by [Valve](https://www.valvesoftware.com/en/about), it runs natively on Linux, and it provides you with the Proton compatibility tool.

To install Steam use Discover, or run the following terminal command:

    sudo dnf install steam

[Lutris](https://lutris.net/) is a preservation platform for games, originally developed by Mathieu Comandon a.k.a. [strycore](https://github.com/strycore), and maintained by its community. It is based on [Wine](https://www.winehq.org/), and you can use Lutris to access many different game libraries, including GOG Galaxy, EPIC Games Store, Origin, Battle.NET, and Ubisoft.

Lutris also provides customized installation scripts for your games, based on the library it is located in. These can be browsed in its official website.

To install lutris use Discover, or use the following terminal command:

    sudo dnf install lutris

[Heroic](https://heroicgameslauncher.com/) is another game launcher available for Linux, originally created by Flávio Fearn a.k.a. [flavioislima](https://github.com/flavioislima). It looks much more modern that Lutris, and it simplifies the installation process of games. With Heroic you can access your GOG Galaxy, EPIC Game Store, and Amazon Prime libraries.

Heroic is distributed as a Flatpak, and you can install it directly from Discover, or with the following command:

    flatpak install heroic

&nbsp;

### Gaming Utilitites

To monitor my system performance while playing games, and to enhance some of the graphical and hardware capabilities of my system, I use MangoHud, GameMode, VKBasalt, and CoreCTL.

[MangoHud](https://github.com/flightlessmango/MangoHud) is a system overlay for Vulkan and OpenGL applications, developed by [Flightlessmango](https://github.com/flightlessmango). With MangoHud, you can monitor your GPU, CPU, and system performance while gaming. It also allows you to set performance options for graphical applications (i.e. FPS limiters, VSync, texture filtering, etc.), it is highly customizable, and it resembles the Riviatuner Statistics Sever featured in the MSI Afterburner.

To install MangoHud use Discover, or run the following terminal command:

    sudo dnf install mangohud

> 💡 **Tip**: The customization of MangoHud is done manually, modifying the parameters written in its .conf file. If this seems too cumbersome for you, you can install and use [GOverlay](https://github.com/benjamimgois/goverlay), which is a graphical application that helps with the MangoHud configuration.

As defined in its GitHub repository, "[GameMode](https://github.com/FeralInteractive/gamemode) is a daemon/lib combo for Linux that allows games to request a set of optimizations be temporarily applied to the host OS and/or a game process". GameMode is developed by [Feral Interactive](https://www.feralinteractive.com/en/about/) and, in simpler terms, allows the game to establish processing priority in the OS, despite power-saving or any other configuration.

I rarely use GameMode myself but, when games need a little bit more of processing power, it is a very effective tool.

To install GameMode use Discover, or run the following terminal command:

    sudo dnf install gamemode

[VKBsalt](https://github.com/DadSchoorse/vkBasalt) is a Vulkan post-processing application, developed by Georg Lehmann a.k.a. [DadSchoorse](https://github.com/DadSchoorse). VKBasalt enhances some graphical attributes of games, such as contrast, sharpening, denoising, ant-aliasing, and 3D colors.

To install VKBasalt use Discover, or use the following terminal command:

    sudo dnf install vkbasalt

[CoreCtrl](https://gitlab.com/corectrl/corectrl) is a graphical application that allows to customize hardware behavior by creating application profiles. It is developed by [Juan Palacios](https://gitlab.com/jpalaciosdev), and it resembles Adrenalin by AMD Software.

With CoreCtrl you can apply GPU or VRAM overclock/undervolt, or modify the default cooling parameters in the GPU driver.

To install CoreCtrl run the following terminal command:

    sudo dnf install corectrl

&nbsp;

## 🤖 Artificial Intelligence <a name="ai"></a>

Thanks to the release and adoption of ChatGPT, the popularity [LLM](https://www.geeksforgeeks.org/large-language-model-llm/)s and generative AI has exploded. Linux has not been away from this, and there are many open source generative AI models that we can install locally and use in Fedora Workstation.

First, we will need a CLI application called [Ollama](https://ollama.com/) 🦙, developed by Jeffrey Morgan  a.k.a. [jmorganca](https://github.com/jmorganca). Ollama acts as a container to install, store, and execute multiple LLM models directly in your GPU. Ollama runs natively in the terminal, but it can be integrated into many other applications.

To install Ollama run the following terminal command:

    curl -fsSL https://ollama.com/install.sh | sh 

With Ollama installed, the only thing missing is to install some LLMs. The LLMs that I commonly use are:

- [Llama3](https://llama.meta.com/llama3/): Llama3 is an 8B (i.e. 8 billion parameters) open source model developed by [Meta](https://about.meta.com). To install or run Llama3, use the following terminal command:

       ollama run llama3

- [Gemma](https://ai.google.dev/gemma/): Gemma is a 7B opens source model, developed by [Google DeepMind](https://deepmind.google/) and inspired by [Gemini](https://gemini.google.com/?hl=en-GB) models. To install or run Gemma, use the following terminal command:

       ollama run gemma       

- [Mistral](https://mistral.ai/): Mistral is a 7B open source model developed by [Mistral AI](https://mistral.ai/company/). To install or run Mistral, use the following terminal command:

       ollama run mistral

- [Phi3](https://azure.microsoft.com/en-us/blog/introducing-phi-3-redefining-whats-possible-with-slms/): Phi3 is a lightweight 4B open source model developed by [Microsoft](https://www.microsoft.com/). To install or run Phi3, use the following command:

       ollama run phi3

> 💡 **Tip**: The use of Ollama in the terminal could be a bit daunting or impractical for some people. If this is your case, you can also use [WebUI](https://github.com/open-webui/open-webui) by [Open WebUI](https://github.com/open-webui), a browser-based interface that allows you to access and manage your local LLMs. To set it up, I recommend [this WebUI tutorial](https://www.youtube.com/watch?v=Wjrdr0NU4Sk) by [NetworkChuck](https://www.youtube.com/@NetworkChuck). Kudos to him for this project! ☕

&nbsp;

## ⚒️ Applications <a name="app"></a>

To finish this repository, here I share the applications I commonly use in Fedora Workstation (i.e. those that I have not already mentioned in previous sections of the repository). They are divided into two categories: command line interface (CLI) applications and graphical user interface (GUI) applications.

&nbsp;

### CLI Applications

CLI applications are programs that are run and executed directly in the terminal console.

#### Audio
- [Alsamixer](https://www.alsa-project.org/wiki/Main_Page): controls the audio interfaces in your system, and their respective output levels. It is developed by the [Alsa Team](https://www.alsa-project.org/wiki/Alsa_Team), and it usually comes pre-installed in Fedora Workstation. Otherwise, you can install it with the terminal command:

      sudo dnf alsa-tools

> 💡 **Tip**: If when changing audio outputs there are no sounds being played, exploring the output levels via alsamixer is one of the most common tools to fix this kind of issue.

#### Graphics and Photos
- [Image Magick](https://imagemagick.org/): a rather robust toolkit to create, edit, compose or convert images. It is developed by [Image Magic Studio LLC](https://github.com/ImageMagick), and comes pre-installed in Fedora Workstation. Otherwise, you can [download its RPM package directly from their website](https://imagemagick.org/script/download.php#linux).

#### Video and Animation
- [FFMPEG](https://ffmpeg.org/): a really powerful multimedia framework that decodes, transcodes, streams, filters, and plays most video and audio files. It is developed by the FFMPEG Team, and it is a must when working in Multimedia!. To install, use the terminal command:

       sudo dnf install ffmpeg --allowerasing

> ⚠️ **Warning**: The previous command requires that RPM fusion is already installed in your system.

#### Text Editors

- [Neovim](https://neovim.io/): an extensible and very customizable keyboard-based text editor, developed and maintained by [its community](https://github.com/neovim). [This Fireship video showcase it quite well](https://www.youtube.com/watch?v=c4OyfL5o7DU). To install Neovim, use the following terminal:

      sudo dnf install nvim

- [Bat](https://github.com/sharkdp/bat): a clone of cat (native Linux concatenate tool), that offers syntax highlighting and git integration. It is developed by David Peter a.k.a. [Sharkdp](https://github.com/sharkdp), and can be installed with the following terminal command:

      sudo dnf install bat      

#### Documents and Office Suites

- [Pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/): a practical toolkit to edit .pdf files. It is developed by [PDF Labs](https://www.pdflabs.com/), and it can be installed using the following terminal command:

      sudo dnf install pdftk

#### System Tools

- [Ranger](https://github.com/ranger/ranger): a terminal-based file manager, with VI bindings. It is developed by the [Ranger team](https://github.com/ranger) and it can be installed running the following terminal command:

      sudo dnf install ranger      

- [Neofetch](https://github.com/dylanaraps/neofetch): a customizable system information utility. It is developed by "Dylan" a.k.a. [dylanaraps](https://github.com/dylanaraps) and can be installed with the terminal command:

      sudo dnf install neofetch

- [CPUfetch](https://github.com/Dr-Noob/cpufetch): a customizable CPU information utility. It is developed by [Dr-Noob](https://github.com/Dr-Noob), and it can be installed using the terminal command:

      sudo dnf install cpufetch

#### Miscellaneous

- [CMatrix](https://github.com/abishekvashok/cmatrix): renders a matrix-like console animation in your terminal. It is developed by Abishek V Ashok a.k.a. [abishekvashok](https://github.com/abishekvashok), and it can be installed running:

      sudo dnf install cmatrix

&nbsp;

### GUI Applications
These are the more "traditional" applications I currently use in my Fedora Workstation. Most of them are installed via Discover.

#### Audio
- [Ardour](https://ardour.org/): a very complete digital audio workstation (DAW), that can record, edit and mix multiple audio tracks. It was originally developed by Paul Davis, and it is a good alternative to Adobe Audition.
- [Audacity](https://www.audacityteam.org/): a light-weight recording and audio editing software. Useful to make minor adjustments and apply audio effects to audio tracks. It is developed by the [Muse Group](https://www.mu.se/).
- [Easy Effects](https://github.com/wwmm/easyeffects): a very complete audio effects utilities for [PipeWire](https://www.pipewire.org/). It is developed by Wellington Wallace a.k.a. [wwmm](https://github.com/wwmm).
- [LMMS](https://lmms.io/): another great DAW, that shines at sampling and synthesizing audio. It was originally developed by Paul Giblock and Tobias Junghans.
- [MuseScore](https://musescore.org/en) is musical notation software, that writes and plays music sheets. It is developed by the [Muse Group](https://www.mu.se/).
- [Guitarix](https://guitarix.org/): a modular virtual amplifier and effect library for Linux. It is primarily developed by Hermann Meyer a.k.a. [brummer10](https://github.com/brummer10).

#### Graphics and Photos
- [Inkscape](https://inkscape.org/): a very robust vector graphics editor. It is developed by its own community. It is a good alternative to Adobe Illustrator.
- [GIMP](https://www.gimp.org/): a very robust image (bitmap) manipulation program. It was originally developed by Spencer Kimball and Peter Mattis, and it is a good alternative to Adobe Photoshop.
- [Krita](https://krita.org/en/): a fully-featured digital painting and illustration program. It is developed by the [Krita Foundation](https://krita.org/en/about/krita-foundation/) and [KDE](https://kde.org/). 
- [Darktable](https://www.darktable.org/): a RAW developer and photo editing software. It was originally developed by Johannes Hanika.
- [Curtail](https://github.com/Huluti/Curtail): a simple image compressor for .png, .jpeg, .webp, and .svg files. It is developed by  Hugo Posnic a.k.a. [Huluti](https://github.com/Huluti).

#### Video and Animation.
- [Kdenlive](https://kdenlive.org/): a robust non-linear video editor. It was originally developed by Jason Wood, and it is currently maintained by [KDE](https://kde.org/). It is a good alternative to Adobe Premiere.
- [OBS](https://obsproject.com/): a full-featured screencasting and streaming application. It developed and maintained by Lain Bailey, together with it is large community.
- [Natron](https://natrongithub.github.io/): a simple and powerful node-based video compositing software for VFX and Motion Graphics. It was originally developed by Alexandre Gauthier and Frédéric Devernay, and it is currently maintained by its community. It could be a good alternative to Adobe After Effects.
- [OpenToonz](https://opentoonz.github.io/e/): a rather robust 2D animation suite. It is based on Toonz Studio Ghibli Version. It is developed and maintained by [Dwango](https://dwango.co.jp/).
- [VLC Media Player](https://www.videolan.org/vlc/): a very robust multimedia player and framework. It is developed by the [VideoLAN Organization](https://www.videolan.org/).

#### 3D and Game Engines:
- [Blender](https://www.blender.org/): is a very robust 3D studio software that features rendering, modelling, sculpting, animation, rigging, texturing, rendering, and VFX. It is developed and sustained by the [Blender Foundation](https://www.blender.org/about/foundation/).
- [Godot](https://godotengine.org/): a cross-platform 2D and 3D game engine. It was originally developed by Juan Linietsky and Ariel Manzur, and it is maintained by its community.
- [O3DE](https://o3de.org/): an full-featured open-source 3D game engine, derived from Cry Engine and Lumberyard. It is developed nad maintained by the O3DE foundation.
- [Unreal Engine](https://www.unrealengine.com/en-US): the best professional game engine available to date. It is developed by [EPIC Games](https://www.epicgames.com/site/en-US/home?sessionInvalidated=true).
- [FreeCAD](https://www.freecad.org/): a full-featured parametric 3D modeler. It was originally developed by Jürgen Riegel, Werner Mayer, and Yorik van Havre, and it is maintained by its community.
- [Material Maker](https://www.materialmaker.org/): a procedural texture and 3D model painting tool. It is based on the Godot game engine and it is developed by [Rodz Labs](https://github.com/RodZill4).
- [Armorpaint](https://armorpaint.org/download): a 3D painting software that allows painting textures directly on  models. It is developed by [Lubos Lenco](https://github.com/luboslenco) and the [Armory 3D group](https://github.com/armory3d).

#### Documents and Office Suites.
- [LibreOffice](https://www.libreoffice.org/): a very complete office suite. It is developed and sustained by [The Document Foundation](https://www.documentfoundation.org/), and it is a great alternative to Microsoft Office. 
- [TexMaker](https://www.xm1math.net/texmaker/): a simple but powerful Latex editor. It is developed by Pascal Brachet.
- [Ghostwriter](https://ghostwriter.kde.org/): a simple and effective Markdown editor, developed by [KDE](https://kde.org/).
- [Okular](https://okular.kde.org/): a very complete document viewer. It is developed by [KDE](https://kde.org/).
- [Scribus](http://www.scribus.us/): a rather comprehensive desktop publishing application. It is a good alternative to Adobe InDesign, and it is developed by the Scribus team.

#### Programming IDEs.
- [Spyder](https://www.spyder-ide.org/): a python IDE focused on data science, that features the IPython interpreter. It was originally developed by Pierre Raybaut, but it is currently maintained by its community.
- [Jupyter Lab](https://jupyter.org/): an interactive development environment that mixes Python, Julia, R, and Markdown. It is developed by the [Project Jupyter](https://jupyter.org/about).
- [Code::Blocks](https://www.codeblocks.org/): a full-featured IDE for C, C++ and Fortran. It is developed and sustained by the Code::Blocks team.
- [Eclipse](https://eclipseide.org/): a very robust Java IDE developed by the [Eclipse Foundation](https://www.eclipse.org/).
- [DBeaver](https://dbeaver.io/): a cross-platform database manager. It was originally developed by Serge Rider, but it is currently maintained by its community.

#### System Tools
- [Metadata Cleaner](https://apps.gnome.org/MetadataCleaner/): a simple application to remove metadata information from files. It is developed and maintained by Romain Vigier a.k.a [rmnvgr](https://gitlab.com/rmnvgr).
- [KDE Connect](https://kdeconnect.kde.org/): enables communication, interaction, and data transfer between mobile devices and computers. It is developed by [KDE](https://kde.org/).
- [QEMU](https://www.qemu.org/): a type-1 hypervisor for OS virtualization. It was originally developed by [Fabrice Bellard](https://en.wikipedia.org/wiki/Fabrice_Bellard), and it is currently maintained by the QEMU team.