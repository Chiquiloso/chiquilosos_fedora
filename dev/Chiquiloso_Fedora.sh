#!/bin/bash

# ====================
# DECALARING FUNCTIONS
# ====================

function confirm_installation()
{
    while true; do
        read -p "[Chiquiloso.prs.cns]: So, what do you say choom? shall we tune your toaster up? [Y/N] " answer
        case $answer in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            [Cc]* ) exit;;
            * ) echo -e "[Chiquiloso.prs.cns]: You gonk 😑. \n[Chiquiloso.prs.cns]: Just type (y) for YES, or (n) for NO!";;
        esac
    done
}

random_ascii() {
  local chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()"
  for ((i=0; i<100; i++)); do
    echo "${chars:$(( RANDOM % ${#chars} )):200}"
    sleep 0.05
  done
}


function script_intro()
{
  clear
  echo -e "-------------------------------------------------------\n--- WELCOME TO THE CHIQUILOSO'S FEDORA SETUP SCRIPT ---\n-------------------------------------------------------\n"
  sleep 5
  echo "- Initializing Script..."
  sleep 3
  echo "- Securing Cyberspace Connection..."
  sleep 3
  echo "- Connecting into Cyberspace..."
  sleep 3
  echo "- Breaking ICEs..."
  sleep 4
  echo "- Breaching Blackwall..."
  sleep 6
  echo "[WARNING]: R.A.B.I.D.S encountered. Closing connection..."
  sleep 5
  echo "[WARNING]: System breached. Connection established by unknown R.A.B.I.D.S."
  sleep 5
  echo "[KERNEL-RUNTIME-ERROR]: systemctl status service.error: overwriting systemd."
  sleep 2
  echo "[KERNEL-RUNTIME-ERROR]: systemctl status service.error: overwriting systemd."
  sleep 2
  echo "[KERNEL-RUNTIME-ERROR]: systemctl status service.error: overwriting systemd."
  sleep 2
  echo "[KERNEL-RUNTIME-ERROR]: systemctl status service.error: overwriting systemd."
  sleep 2
  random_ascii
  clear
}


function installation_intro()
{
  echo -e "                ****************\n            **                    **\n        ***          **              ***\n      **             *****              **\n    **             * ****** *             **\n   *            **** ****** ****            *\n  *           ****** ****** *****            *\n *            ****** ****** *****             *\n**            ****** ****** *****             **\n*             ****** ****** *****              *\n*             ****** ****** *****              *\n*             ****** ****** *****              *\n**            ****** ****** *****             **\n *            ****** ****** *****             *\n  *            ***** ****** ****             *\n   *              ** ****** **              *\n    **               ******               **\n      **               ****             **\n        ***              **          ***\n            **                    **\n                ****************\n"

  sleep 5

  echo -e "*******************************************\n* R.A.B.I.D.S STR_ID: CHIQUILOSO'S FEDORA *\n*******************************************"
  echo -e "CHIQUILOSO'S FEDORA\nDeveloped by Chiquiloso.\nVersion 1.01.06.2024.\nTested in: Fedora 40.\n"
  sleep 5

  echo -e "\n*** INITIALIZING PERSONALITY CONSTRUCT ***\n"
  sleep 4
  echo "[Chiquiloso.prs.cns]: Wow... hi there choom! 👋"
  sleep 4
  echo "[Chiquiloso.prs.cns]: I released this R.A.B.I.D.S in the far end of Cyberspace. Yet, you found it... preem work deckhead!"
  sleep 10
  read -p "[Chiquiloso.prs.cns]: You there choom?, press enter and let me know you haven't flatlined! 😱"
  echo -e "\n[Chiquiloso.prs.cns]: Nova!, don't scare me like that! 😭"
  sleep 4
  echo "[Chiquiloso.prs.cns]: Alright. Before we start, let's check your system up 🔍"
  sleep 4
  echo "[Chiquiloso.prs.cns]: R.A.B.I.D.S, do a quick system check-up."
  sleep 6
  echo -e "\n*** RETRIEVEING SYSTEM INFORMATION ***\n"
  sleep 3
  lspci
  sleep 3
  lscpu
  sleep 3
  echo -e "\n[Chiquiloso.prs.cns]: Let's see what you got... 🤔"
  sleep 6
  echo -e "[Chiquiloso.prs.cns]: Uhmmm... I guess you call that a 'computer' in meatspace nowadays... 😅"
  sleep 5
  echo "[Chiquiloso.prs.cns]: Ok, R.A.B.I.D.S, prepare system upgrade for our choom here."
}


function confirming_upgrade()
{
  echo -e "\n*** PREPARING SYSTEM UPGRADE ***\n"
  sleep 2

  echo -e  "* DESCRIPTION: This script automatically installs some of the key packages to set up Fedora for game development, multimedia content creation, and gaming. It follows the resources disclosed in the project https://gitlab.com/Chiquiloso/chiquilosos_fedora .\n"

  read -p "Press enter to continue..."

  echo -e "\nSpecifically, this scrip will execute the following processes:\n\t- Install RPM Fusion.\n\t- Enable RPM Fusion in Discover.\n\t- Enable propietary multimedia codecs and utilitites.\n\t- Enable Flatpak.\n\t- Install bpytop.\n\t- Install liquidctl.\n\t- Install openrgb.\n\t- Install Steam.\n\t- Install Lutris.\n\t- Install Heroic (Flatpak).\n\t- Install MangoHud.\n\t- Install GameMode.\n\t- Install VKBasalt.\n\t- Install CoreCtrl.\n\t- Install Ollama.\n\t- Install FFMPEG.\n\t- Install NeoVim.\n\t- Install pdftk.\n\t- Install Neofetch.\n\t- Install cpufetch.\n\t- Install Ardour (Flatpak).\n\t- Install Audacity (Flatpak).\n\t- Install Easy Effects.\n\t- Install InkScape.\n\t- Install GIMP.\n\t- Install Krita.\n\t- Install Kdenlive.\n\t- Install OBS (Flatpak).\n\t- Install VLC.\n\t- Install Blender.\n\t- Install Godot.\n\t- Install Ghostwriter.\n\t- Install Spyder.\n\t- Install Jupyter Lab.\n\t- Install Code::Blocks.\n\t- Install Bluefish.\n\t- Install QEMU.\n\t- Update your system.\n"

  read -p "Press enter to continue..."

  echo -e "\n[Chiquiloso.prs.cns]: Remember choom, by executing this script you:\n\t1. Are no gonk and you know what you're doing.\n\t2. Have read the GitLab project and you are aware of the processes described above 👆.\n\t3. Understand the project license and assume the responsibility of executing this script 🫡.\n"
}

function installing_upgrade()
{
  echo "[Chiquiloso.prs.cns]: Nova!, R.A.B.I.D.S upgrade this system!"
  sleep 2
  echo -e "\n*** UPGRADING SYSTEM ***\n"
  sleep 2

  echo -e "\n*** INSTALLING RPM FUSION ***\n"
  sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  sudo dnf config-manager --enable fedora-cisco-openh264

  echo -e "\n*** ENABLING RPM FUSION IN DISCOVER***\n"
  sudo dnf groupupdate core

  echo -e "\n*** ENABLING PROPIETARY CODECS/UTILITITES ***\n"
  sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
  sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
  sudo dnf install lame\* --exclude=lame-devel
  sudo dnf groupupdate sound-and-video
  sudo dnf group upgrade --with-optional Multimedia

  echo -e "\n*** ENABLING FLATPAK ***\n"
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

  echo -e "\n*** INSTALLING BPYTOP ***\n"
  sudo dnf install bpytop

  echo -e "\n*** INSTALLING LIQUIDCTL ***\n"
  sudo dnf install liquidctl

  echo -e "\n*** INSTALLING OPENRGB ***\n"
  sudo dnf install openrgb

  echo -e "\n*** INSTALLING STEAM ***\n"
  sudo dnf install steam

  echo -e "\n*** INSTALLING LUTRIS ***\n"
  sudo dnf install lutris

  echo -e "\n*** INSTALLING HEROIC ***\n"
  flatpak install flathub com.heroicgameslauncher.hgl

  echo -e "\n*** INSTALLING MANGOHUD ***\n"
  sudo dnf install mangohud

  echo -e "\n*** INSTALLING GAMEMODE ***\n"
  sudo dnf install gamemode

  echo -e "\n*** INSTALLING VKBASALT ***\n"
  sudo dnf vkbasalt

  echo -e "\n*** INSTALLING CORECTRL ***\n"
  sudo dnf corectrl

  echo -e "\n*** INSTALLING CORECTRL ***\n"
  sudo dnf corectrl

  echo -e "\n*** INSTALLING OLLAMA ***\n"
  curl -fsSL https://ollama.com/install.sh | sh

  echo -e "\n*** INSTALLING FFMPEG ***\n"
  sudo dnf install ffmpeg --allowerasing

  echo -e "\n*** INSTALLING NEOVIM ***\n"
  sudo dnf install nvim

  echo -e "\n*** INSTALLING PDFTK ***\n"
  sudo dnf install pdftk

  echo -e "\n*** INSTALLING NEOFETCH ***\n"
  sudo dnf install neofetch

  echo -e "\n*** INSTALLING CPUFETCH ***\n"
  sudo dnf install cpufetch

  echo -e "\n*** INSTALLING ARDOUR ***\n"
  flatpak install flathub org.ardour.Ardour

  echo -e "\n*** INSTALLING AUDACITY ***\n"
  flatpak install flathub org.audacityteam.Audacity

  echo -e "\n*** INSTALLING EASY EFFECTS ***\n"
  sudo dnf install easyeffects

  echo -e "\n*** INSTALLING INKSCAPE ***\n"
  sudo dnf install inkscape

  echo -e "\n*** INSTALLING GIMP ***\n"
  sudo dnf install gimp

  echo -e "\n*** INSTALLING KRITA ***\n"
  sudo dnf install krita

  echo -e "\n*** INSTALLING KDENLIVE ***\n"
  sudo dnf install kdenlive

  echo -e "\n*** INSTALLING OBS ***\n"
  flatpak install flathub com.obsproject.Studio

  echo -e "\n*** INSTALLING VLC ***\n"
  sudo dnf install vlc

  echo -e "\n*** INSTALLING BLENDER ***\n"
  sudo dnf install blender

  echo -e "\n*** INSTALLING GODOT ***\n"
  sudo dnf install godot

  echo -e "\n*** INSTALLING GHOSTWRITER ***\n"
  sudo dnf install ghostwriter

  echo -e "\n*** INSTALLING SPYDER ***\n"
  sudo dnf install spyder

  echo -e "\n*** INSTALLING JUPYTER LAB ***\n"
  sudo dnf install jupyter

  echo -e "\n*** INSTALLING CODEBLOCKS ***\n"
  sudo dnf install codeblocks

  echo -e "\n*** INSTALLING BLUE FISH ***\n"
  sudo dnf install bluefish

  echo -e "\n*** INSTALLING QEMU ***\n"
  dnf install @virtualization

  echo -e "\n*** UPDATING RPMS ***\n"
  sudo dnf upgrade

  echo -e "\n*** UPDATING Flatpaks ***\n"
  flatpak update
}


# ==================
# RUNNING THE SCRIPT
# ==================

script_intro

sleep 5

installation_intro

sleep 5

confirming_upgrade

sleep 5

if confirm_installation; then
  installing_upgrade
else
  echo -e "The installation has flatlined. Return to meatspace Choom!"
  fi

